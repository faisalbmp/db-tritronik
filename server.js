var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    bodyParser = require('body-parser'),
    controller = require('./controller');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// const  router  =  express.Router();

var routes = require('./routes');
routes(app);

app.listen(port);
console.log('connect to port : ' + port);