'use strict';
// const router = require('express').Router();

module.exports = function(app) {
    var todoList = require('./controller');

    // router.get('/',todolist.index);


    app.route('/api/machine_data/problem')
        .get(todoList.problem);

    app.route('/api/machine_data/service')
        .get(todoList.service);
    // app.route('/users/:user_id')
    //     .get(todoList.findUsers);
    // app.route('/users/')
    //     .post(todoList.createUsers);
};