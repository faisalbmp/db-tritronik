'use strict';

var response = require('./res');
var connection = require('./conn');

exports.problem = function(req, res) {
    connection.query('SELECT machine_data.* FROM ticket , machine_data where machine_data.id = ticket.machine_id AND ticket.problem in ("mati","no respond")', function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok(rows, res)
        }
    });
};


exports.service = function(req, res) {
    connection.query('SELECT machine_data.name,service.type FROM ticket , service,machine_data where service.id = ticket.service_id and machine_data.id = ticket.machine_id ', function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok(rows, res)
        }
    });
};

exports.index = function(req, res) {
    response.ok("Hello!", res)
};
exports.createUsers = function(req, res) {
        // res.status(200).send({ access_token:  '' });
    var name = req.body.name;
    var createddate = req.body.createddate;
    console.log(name);
    connection.query('INSERT INTO shopping (name,createddate) values (?,?)',
    [ name, createddate ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok("Berhasil menambahkan user!", res)
        }
    });

};
exports.findUsers = function(req, res) {
    
    var user_id = req.params.user_id;
        // response.ok(user_id, res);


    connection.query('SELECT * FROM shopping where id = ?',
    [ user_id ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok(rows, res)
        }
    });
};